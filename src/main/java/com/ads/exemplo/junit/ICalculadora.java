package com.ads.exemplo.junit;

public interface ICalculadora {
	int somar(int a, int b);

	int subtrair(int a, int b);

	int multiplicar(int a, int b);

	int dividir(int a, int b) throws Exception;

	boolean validarNumerosIguais(int a, int b);
}
