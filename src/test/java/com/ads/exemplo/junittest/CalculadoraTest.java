package com.ads.exemplo.junittest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ads.exemplo.junit.Calculadora;
import com.ads.exemplo.junit.ICalculadora;

/*  
    @BeforeClass – Sometimes several tests need to share computationally expensive setup (like logging into a database). While this can compromise the independence of tests, sometimes it is a necessary optimization. Annotating a public static void no-arg method with @BeforeClass causes it to be run once before any of the test methods in the class. The @BeforeClass methods of superclasses will be run before those the current class.
    @Before – When writing tests, it is common to find that several tests need similar objects created before they can run. Annotating a public void method with @Before causes that method to be run before the Test method. The @Before methods of superclasses will be run before those of the current class.
    @After – If you allocate external resources in a Before method you need to release them after the test runs. Annotating a public void method with @After causes that method to be run after the Test method. All @After methods are guaranteed to run even if a Before or Test method throws an exception. The @After methods declared in superclasses will be run after those of the current class.
    @Test – The Test annotation tells JUnit that the public void method to which it is attached can be run as a test case. To run the method, JUnit first constructs a fresh instance of the class then invokes the annotated method. Any exceptions thrown by the test will be reported by JUnit as a failure. If no exceptions are thrown, the test is assumed to have succeeded.
    @Ignore – Sometimes you want to temporarily disable a test or a group of tests. Methods annotated with Test that are also annotated with @Ignore will not be executed as tests. Also, you can annotate a class containing test methods with @Ignore and none of the containing tests will be executed. Native JUnit 4 test runners should report the number of ignored tests along with the number of tests that ran and the number of tests that failed.
*/
public class CalculadoraTest {
	private static ICalculadora calculadora;

	@BeforeClass
	public static void iniciarCalculadora() {
		calculadora = new Calculadora();
	}

	@Before
	public void executarAntesDeCadaTeste() {
		System.out.println(
				"Este método é executado antes de cada teste. Pode ser necessário para fazer um setup antes de executar algum teste ou fazer alguma verificação na base de dados");
	}

	@After
	public void executarDepoisDeCadaTeste() {
		System.out.println(
				"Este método é executado depois de cada teste. Pode ser necessário para liberar algum recurso após a execução do teste.");
	}

	@Test
	public void testSoma() {
		int result = calculadora.somar(3, 4);

		assertEquals(7, result);
	}

	@Test
	public void testDivisao() {
		try {
			int result = calculadora.dividir(10, 2);

			assertEquals(5, result);
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	}

	@Test(expected = Exception.class)
	public void testDivisaoException() throws Exception {
		calculadora.dividir(10, 0);
	}

	@Test
	public void testNumerosIguais() {
		boolean result = calculadora.validarNumerosIguais(20, 20);

		assertTrue(result);
	}

	@Ignore
	@Test
	public void testSubtracao() {
		int result = 322 - 48;

		assertTrue(result == 284);
	}
}
